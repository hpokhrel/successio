$(document).ready(function() {
  // Initialize slick slider
  $('.bg_slider').slick({
    autoplay: true,
    arrows: false,
    dots: false
  });

  // Initialize dots with inactive SVG icon
  $('.dot-item').each(function() {
    $(this).html('<svg width="8" height="8" viewBox="0 0 8 8" fill="none" xmlns="http://www.w3.org/2000/svg"><rect width="8" height="8" rx="3" fill="#D9D9D9"/></svg>');
  });

  // Set initial dot state
  $('.dot-item:first-child').addClass('active');
  $('.dot-item:first-child').html('<svg width="12" height="12" viewBox="0 0 12 12" fill="none" xmlns="http://www.w3.org/2000/svg"><rect x="2" y="2" width="8" height="8" rx="3" fill="#D9D9D9"/><rect x="0.5" y="0.5" width="11" height="11" rx="4.5" stroke="#D9D9D9"/></svg>');

  // Switch SVG icons on dots
  $('.bg_slider').on('beforeChange', function(event, slick, currentSlide, nextSlide) {
    // Remove active class from previous dot item
    $('.dot-item[data-index="' + currentSlide + '"]').removeClass('active');
    // Add active class to next dot item
    $('.dot-item[data-index="' + nextSlide + '"]').addClass('active');
    // Update SVG icons
    $('.dot-item').each(function() {
      if ($(this).hasClass('active')) {
        $(this).html('<svg width="12" height="12" viewBox="0 0 12 12" fill="none" xmlns="http://www.w3.org/2000/svg"><rect x="2" y="2" width="8" height="8" rx="3" fill="#D9D9D9"/><rect x="0.5" y="0.5" width="11" height="11" rx="4.5" stroke="#D9D9D9"/></svg>');
      } else {
        $(this).html('<svg width="8" height="8" viewBox="0 0 8 8" fill="none" xmlns="http://www.w3.org/2000/svg"><rect width="8" height="8" rx="3" fill="#D9D9D9"/></svg>');
      }
    });
  });

  // Click event for dot items
  $('.dot-item').click(function() {
    var dotIndex = $(this).data('index');
    var currentIndex = $('.bg_slider').slick('slickCurrentSlide');

    if (dotIndex > currentIndex) {
      $('.bg_slider').slick('slickNext', dotIndex - currentIndex);
    } else if (dotIndex < currentIndex) {
      $('.bg_slider').slick('slickPrev', currentIndex - dotIndex);
    }
  });
});



$(document).ready(function() {
  $('.ul_slider').slick({
    dots: false,
    arrows: false,
    infinite: true,
    speed: 1000,
    slidesToShow: 3,
    slidesToScroll: 3,
    autoplay: true,
    autoplaySpeed: 5000,
    // fade: true,
    cssEase: 'ease',

    responsive: [
      {
          breakpoint: 1199,
          settings: {
              dots: false,
              arrows: false,
              slidesToShow: 1,
              slidesToScroll: 1
          }
      },
      {
          breakpoint: 991,
          settings: {
              dots: false,
              arrows: false,
              slidesToShow: 1,
              slidesToScroll: 1
          }
      },
      {
          breakpoint: 767,
          settings: {
              dots: false,
              arrows: false,
              slidesToShow: 1,
              slidesToScroll: 1
          }
      }
  ]
  });

  // Set active class and dot state on dot click
  $('.dot-card').on('click', function() {
    // Get dot index
    var dotIndex = $(this).index();

    // Get current slide index
    var currentIndex = $('.ul_slider').slick('slickCurrentSlide');

    // Move slider to clicked dot
    if (dotIndex > currentIndex) {
      $('.ul_slider').slick('slickNext', dotIndex - currentIndex);
    } else if (dotIndex < currentIndex) {
      $('.ul_slider').slick('slickPrev', currentIndex - dotIndex);
    }

    // Set active class on clicked dot
    $('.dot-card').removeClass('active');
    $(this).addClass('active');

    // Set dot state
    $('.dot-card').each(function() {
      if ($(this).hasClass('active')) {
        $(this).html('<svg width="12" height="12" viewBox="0 0 12 12" fill="none" xmlns="http://www.w3.org/2000/svg"><rect x="2" y="2" width="8" height="8" rx="3" fill="#D9D9D9"/><rect x="0.5" y="0.5" width="11" height="11" rx="4.5" stroke="#D9D9D9"/></svg>');
      } else {
        $(this).html('<svg width="8" height="8" viewBox="0 0 8 8" fill="none" xmlns="http://www.w3.org/2000/svg"><rect width="8" height="8" rx="3" fill="#D9D9D9"/></svg>');
      }
    });
  });

  // Set initial dot state
  $('.dot-card:first-child').addClass('active');
  $('.dot-card:first-child').html('<svg width="12" height="12" viewBox="0 0 12 12" fill="none" xmlns="http://www.w3.org/2000/svg"><rect x="2" y="2" width="8" height="8" rx="3" fill="#D9D9D9"/><rect x="0.5" y="0.5" width="11" height="11" rx="4.5" stroke="#D9D9D9"/></svg>');
});


  
$(document).ready(function() {

  $('.video-carousel').slick({
    dots: false,
    arrows: true,
    infinite: true,
    speed: 500,
    slidesToShow: 1,
    slidesToScroll: 1,
    autoplay: true,
    autoplaySpeed: 4000,
    responsive: [{
      breakpoint: 768,
      settings: {
        arrows: false
      }
    }]
  });

  
  // Initialize dots with inactive SVG icon
  $('.dot-video').each(function() {
    $(this).html('<svg width="8" height="8" viewBox="0 0 8 8" fill="none" xmlns="http://www.w3.org/2000/svg"><rect width="8" height="8" rx="3" fill="#103051"/></svg>');
  });
  
  // Switch SVG icons on dots
  $('.video-carousel').on('beforeChange', function(event, slick, currentSlide, nextSlide) {
    // Remove active class from previous dot item
    $('.dot-video[data-index="' + currentSlide + '"]').removeClass('active');
    // Add active class to next dot item
    $('.dot-video[data-index="' + nextSlide + '"]').addClass('active');
    // Update SVG icons
    $('.dot-video').each(function() {
      if ($(this).hasClass('active')) {
        $(this).html('<svg width="12" height="12" viewBox="0 0 12 12" fill="none" xmlns="http://www.w3.org/2000/svg"><rect x="2" y="2" width="8" height="8" rx="3" fill="#103051"/><rect x="0.5" y="0.5" width="11" height="11" rx="4.5" stroke="#103051"/></svg>');
      } else {
        $(this).html('<svg width="8" height="8" viewBox="0 0 8 8" fill="none" xmlns="http://www.w3.org/2000/svg"><rect width="8" height="8" rx="3" fill="#103051"/></svg>');
      }
    });
  });
  
  // Click event for dot items
  $('.dot-video').click(function() {
    var dotIndex = $(this).data('index');
    $('.video-carousel').slick('slickGoTo', dotIndex);
  });

  // Set initial dot state
$('.dot-video:first-child').addClass('active');
$('.dot-video:first-child').html('<svg width="12" height="12" viewBox="0 0 12 12" fill="none" xmlns="http://www.w3.org/2000/svg"><rect x="2" y="2" width="8" height="8" rx="3" fill="#103051"/><rect x="0.5" y="0.5" width="11" height="11" rx="4.5" stroke="#103051"/></svg>');

});