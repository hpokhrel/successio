// read more button

function myFunction1() {
    var dots = document.getElementById("dots1");
    var moreText = document.getElementById("moreFirst");
    var btnText = document.getElementById("more1");
    event.preventDefault()
  
    if (dots.style.display === "none") {
      dots.style.display = "inline";
      btnText.innerHTML = "Read more";
      moreText.style.display = "none";
    } else {
      dots.style.display = "none";
      btnText.innerHTML = "Read less";
      moreText.style.display = "inline";
    }
  }

  function myFunction2() {
    var dots = document.getElementById("dots2");
    var moreText = document.getElementById("moreSec");
    var btnText = document.getElementById("more2");
    event.preventDefault()
  
    if (dots.style.display === "none") {
      dots.style.display = "inline";
      btnText.innerHTML = "Read more";
      moreText.style.display = "none";
    } else {
      dots.style.display = "none";
      btnText.innerHTML = "Read less";
      moreText.style.display = "inline";
    }
  }

  function myFunction3() {
    var dots = document.getElementById("dots3");
    var moreText = document.getElementById("moreThird");
    var btnText = document.getElementById("more3");
    event.preventDefault()
  
    if (dots.style.display === "none") {
      dots.style.display = "inline";
      btnText.innerHTML = "Read more";
      moreText.style.display = "none";
    } else {
      dots.style.display = "none";
      btnText.innerHTML = "Read less";
      moreText.style.display = "inline";
    }
  }

// animation started here

function addCssFiles() {
  const styleLink = document.createElement('link');
  styleLink.rel = 'stylesheet';
  styleLink.href = 'assets/css/style.css';
  document.head.appendChild(styleLink);

  const hvrLink = document.createElement('link');
  hvrLink.rel = 'stylesheet';
  hvrLink.href = 'assets/css/hover-min.css';
  document.head.appendChild(hvrLink);

  const custom_animation = document.createElement('link');
  custom_animation.rel = 'stylesheet';
  custom_animation.href = 'assets/css/custom_animation.css';
  document.head.appendChild(custom_animation);

}

addCssFiles();


// animation button js

// const btns = document.querySelectorAll('.btn-primary');

// btns.forEach(btn => {
//   btn.classList.add('hvr-bounce-to-right');
// });


// card animation

const cards = document.querySelectorAll('.ul_slider .card , .ul_slider_all .card');
cards.forEach(card => {
  card.classList.add('hvr-float-shadow');
});

// Aos animation started here:

function loadAos() {
  const animLink = document.createElement('link');
  animLink.rel = 'stylesheet';
  animLink.href = 'assets/css/animation.css';
  document.head.appendChild(animLink);

  const script = document.createElement('script');
  script.src = 'assets/js/animation.js';
  document.body.appendChild(script);

  script.addEventListener('load', function() {
    AOS.init();
    AOS.refresh();
  });
}

window.onload = function() {
  loadAos();

  const imgs = document.querySelectorAll('.ul_slider .card , .ul_slider_all .card , .video_page');
  imgs.forEach((img, index) => {
    img.classList.add('aos-init', 'aos-animate');
    img.setAttribute('data-aos', 'fade-up');
    if(index === 0) {
      img.setAttribute('data-aos-delay', '0');
    } else {
      img.setAttribute('data-aos-delay', `${(index - 1) * 100}`);
    }
  });
};



const h5Element = document.querySelector('span > span > h5');

h5Element.style.transition = 'all 0.3s ease-out';
