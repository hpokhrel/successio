const navbarMenu = document.querySelector('.navbar-menu');
const navbarClose = document.querySelector('.navbar-close');

navbarMenu.addEventListener('click', () => {
  document.body.classList.add('navbar-menu-open');
  const overlay = document.createElement('div');
  overlay.classList.add('overlay');
  document.body.appendChild(overlay);
});

navbarClose.addEventListener('click', (e) => {
  e.preventDefault();
  document.body.classList.remove('navbar-menu-open');
  const overlay = document.querySelector('.overlay');
  document.body.removeChild(overlay);
});


const navbar = document.querySelector('.navbar');

window.addEventListener('scroll', () => {
  const scrollPos = window.scrollY;

  if (scrollPos > 0) {
    navbar.classList.add('navbar-scrolled');
  } else {
    navbar.classList.remove('navbar-scrolled');
  }
});


  // Hamburger js

const navbarMenuIcon = document.querySelector('#navbar-menu-icon, #hello');
const path = navbarMenuIcon.querySelector('path');

navbarMenuIcon.addEventListener('mouseover', () => {
  path.setAttribute('d', 'M2 2H31.4521M2.46238 12H31.4521M2.46238 22H31.4521M7.24631 2H7.64752M2.45215 2H3.00077');
  path.style.transition = 'all 0.3s ease-out';
  path.style.opacity = '1';
});

navbarMenuIcon.addEventListener('mouseout', () => {
  path.setAttribute('d', 'M12 2H31.4521M2.46238 12H31.4521M2.46238 22H31.4521M7.24631 2H7.64752M2.45215 2H3.00077');
  });


// arrow animation Navy

// function arrowNavy () {
// const iconNavy = document.querySelector('#arrowNavy');
// const pathNavy = iconNavy.querySelector('svg path#arrowNavyPathOne');
// const firstRectNavy = document.querySelector('svg rect#arrowNavyRectOne');

// iconNavy.addEventListener('mouseover', () => {
//   pathNavy.setAttribute('d', 'M14.3656 0.367203C14.2497 0.482845 14.1577 0.620206 14.095 0.771422C14.0323 0.922639 14 1.08474 14 1.24845C14 1.41216 14.0323 1.57427 14.095 1.72548C14.1577 1.8767 14.2497 2.01406 14.3656 2.1297L19.2156 6.9797L14.3656 11.8297C14.2498 11.9454 14.158 12.0828 14.0954 12.234C14.0328 12.3852 14.0005 12.5473 14.0005 12.711C14.0005 12.8746 14.0328 13.0367 14.0954 13.1879C14.158 13.3391 14.2498 13.4765 14.3656 13.5922C14.4813 13.7079 14.6187 13.7997 14.7699 13.8624C14.9211 13.925 15.0831 13.9572 15.2468 13.9572C15.4105 13.9572 15.5725 13.925 15.7237 13.8624C15.8749 13.7997 16.0123 13.7079 16.1281 13.5922L21.8656 7.8547C21.9814 7.73906 22.0734 7.6017 22.1361 7.45048C22.1988 7.29927 22.2311 7.13716 22.2311 6.97345C22.2311 6.80974 22.1988 6.64764 22.1361 6.49642C22.0734 6.34521 21.9814 6.20784 21.8656 6.0922L16.1281 0.354703C15.6531 -0.120297 14.8531 -0.120297 14.3656 0.367203Z');
//   pathNavy.style.transition = 'all 0.3s ease-out';
//   pathNavy.style.transform = 'translateX(6px)';
//   pathNavy.style.opacity = '1';
//   firstRectNavy.setAttribute('width', '27');
//   firstRectNavy.style.transform = 'translateX(-8px)';
//   firstRectNavy.style.transition = 'all 0.3s ease-out';
// });

// iconNavy.addEventListener('mouseout', () => {
//   pathNavy.setAttribute('d', 'M14.3656 0.367203C14.2497 0.482845 14.1577 0.620206 14.095 0.771422C14.0323 0.922639 14 1.08474 14 1.24845C14 1.41216 14.0323 1.57427 14.095 1.72548C14.1577 1.8767 14.2497 2.01406 14.3656 2.1297L19.2156 6.9797L14.3656 11.8297C14.2498 11.9454 14.158 12.0828 14.0954 12.234C14.0328 12.3852 14.0005 12.5473 14.0005 12.711C14.0005 12.8746 14.0328 13.0367 14.0954 13.1879C14.158 13.3391 14.2498 13.4765 14.3656 13.5922C14.4813 13.7079 14.6187 13.7997 14.7699 13.8624C14.9211 13.925 15.0831 13.9572 15.2468 13.9572C15.4105 13.9572 15.5725 13.925 15.7237 13.8624C15.8749 13.7997 16.0123 13.7079 16.1281 13.5922L21.8656 7.8547C21.9814 7.73906 22.0734 7.6017 22.1361 7.45048C22.1988 7.29927 22.2311 7.13716 22.2311 6.97345C22.2311 6.80974 22.1988 6.64764 22.1361 6.49642C22.0734 6.34521 21.9814 6.20784 21.8656 6.0922L16.1281 0.354703C15.6531 -0.120297 14.8531 -0.120297 14.3656 0.367203Z');
//   pathNavy.style.transition = 'all 0.3s ease-out';  
//   pathNavy.style.transform = 'translateX(0px)';
//   firstRectNavy.setAttribute('width', '12');
//   firstRectNavy.style.transform = 'translateX(0px)';
//   firstRectNavy.style.transition = 'all 0.3s ease-out';
// });
// }
// arrowNavy();

function arrowNavy() {
  const iconsNavy = document.querySelectorAll('#arrowNavy');
  
  iconsNavy.forEach(iconNavy => {
    const pathNavy = iconNavy.querySelector('svg path#arrowNavyPathOne');
    const firstRectNavy = iconNavy.querySelector('svg rect#arrowNavyRectOne');
    
    iconNavy.addEventListener('mouseover', () => {
      pathNavy.setAttribute('d', 'M14.3656 0.367203C14.2497 0.482845 14.1577 0.620206 14.095 0.771422C14.0323 0.922639 14 1.08474 14 1.24845C14 1.41216 14.0323 1.57427 14.095 1.72548C14.1577 1.8767 14.2497 2.01406 14.3656 2.1297L19.2156 6.9797L14.3656 11.8297C14.2498 11.9454 14.158 12.0828 14.0954 12.234C14.0328 12.3852 14.0005 12.5473 14.0005 12.711C14.0005 12.8746 14.0328 13.0367 14.0954 13.1879C14.158 13.3391 14.2498 13.4765 14.3656 13.5922C14.4813 13.7079 14.6187 13.7997 14.7699 13.8624C14.9211 13.925 15.0831 13.9572 15.2468 13.9572C15.4105 13.9572 15.5725 13.925 15.7237 13.8624C15.8749 13.7997 16.0123 13.7079 16.1281 13.5922L21.8656 7.8547C21.9814 7.73906 22.0734 7.6017 22.1361 7.45048C22.1988 7.29927 22.2311 7.13716 22.2311 6.97345C22.2311 6.80974 22.1988 6.64764 22.1361 6.49642C22.0734 6.34521 21.9814 6.20784 21.8656 6.0922L16.1281 0.354703C15.6531 -0.120297 14.8531 -0.120297 14.3656 0.367203Z');
      pathNavy.style.transition = 'all 0.3s ease-out';
      pathNavy.style.transform = 'translateX(6px)';
      pathNavy.style.opacity = '1';
      firstRectNavy.setAttribute('width', '27');
      firstRectNavy.style.transform = 'translateX(-8px)';
      firstRectNavy.style.transition = 'all 0.3s ease-out';
    });
    
    iconNavy.addEventListener('mouseout', () => {
      pathNavy.setAttribute('d', 'M14.3656 0.367203C14.2497 0.482845 14.1577 0.620206 14.095 0.771422C14.0323 0.922639 14 1.08474 14 1.24845C14 1.41216 14.0323 1.57427 14.095 1.72548C14.1577 1.8767 14.2497 2.01406 14.3656 2.1297L19.2156 6.9797L14.3656 11.8297C14.2498 11.9454 14.158 12.0828 14.0954 12.234C14.0328 12.3852 14.0005 12.5473 14.0005 12.711C14.0005 12.8746 14.0328 13.0367 14.0954 13.1879C14.158 13.3391 14.2498 13.4765 14.3656 13.5922C14.4813 13.7079 14.6187 13.7997 14.7699 13.8624C14.9211 13.925 15.0831 13.9572 15.2468 13.9572C15.4105 13.9572 15.5725 13.925 15.7237 13.8624C15.8749 13.7997 16.0123 13.7079 16.1281 13.5922L21.8656 7.8547C21.9814 7.73906 22.0734 7.6017 22.1361 7.45048C22.1988 7.29927 22.2311 7.13716 22.2311 6.97345C22.2311 6.80974 22.1988 6.64764 22.1361 6.49642C22.0734 6.34521 21.9814 6.20784 21.8656 6.0922L16.1281 0.354703C15.6531 -0.120297 14.8531 -0.120297 14.3656 0.367203Z');
      pathNavy.style.transition = 'all 0.3s ease-out';  
      pathNavy.style.transform = 'translateX(0px)';
      firstRectNavy.setAttribute('width', '12');
      firstRectNavy.style.transform = 'translateX(0px)';
      firstRectNavy.style.transition = 'all 0.3s ease-out';
    });
  });
}

arrowNavy();


// arrow animation Yellow

function arrowYellow() {
  const iconsYellow = document.querySelectorAll('#arrowYellow');
  
  iconsYellow.forEach(iconYellow => {
    const pathYellow = iconYellow.querySelector('svg path#arrowYellowPathOne');
    const firstRectYellow = iconYellow.querySelector('svg rect#arrowYellowRectOne');
    
    iconYellow.addEventListener('mouseover', () => {
      pathYellow.setAttribute('d', 'M14.3656 0.367203C14.2497 0.482845 14.1577 0.620206 14.095 0.771422C14.0323 0.922639 14 1.08474 14 1.24845C14 1.41216 14.0323 1.57427 14.095 1.72548C14.1577 1.8767 14.2497 2.01406 14.3656 2.1297L19.2156 6.9797L14.3656 11.8297C14.2498 11.9454 14.158 12.0828 14.0954 12.234C14.0328 12.3852 14.0005 12.5473 14.0005 12.711C14.0005 12.8746 14.0328 13.0367 14.0954 13.1879C14.158 13.3391 14.2498 13.4765 14.3656 13.5922C14.4813 13.7079 14.6187 13.7997 14.7699 13.8624C14.9211 13.925 15.0831 13.9572 15.2468 13.9572C15.4105 13.9572 15.5725 13.925 15.7237 13.8624C15.8749 13.7997 16.0123 13.7079 16.1281 13.5922L21.8656 7.8547C21.9814 7.73906 22.0734 7.6017 22.1361 7.45048C22.1988 7.29927 22.2311 7.13716 22.2311 6.97345C22.2311 6.80974 22.1988 6.64764 22.1361 6.49642C22.0734 6.34521 21.9814 6.20784 21.8656 6.0922L16.1281 0.354703C15.6531 -0.120297 14.8531 -0.120297 14.3656 0.367203Z');
      pathYellow.style.transition = 'all 0.3s ease-out';
      pathYellow.style.transform = 'translateX(6px)';
      pathYellow.style.opacity = '1';
      firstRectYellow.setAttribute('width', '27');
      firstRectYellow.style.transform = 'translateX(-8px)';
      firstRectYellow.style.transition = 'all 0.3s ease-out';
    });
    
    iconYellow.addEventListener('mouseout', () => {
      pathYellow.setAttribute('d', 'M14.3656 0.367203C14.2497 0.482845 14.1577 0.620206 14.095 0.771422C14.0323 0.922639 14 1.08474 14 1.24845C14 1.41216 14.0323 1.57427 14.095 1.72548C14.1577 1.8767 14.2497 2.01406 14.3656 2.1297L19.2156 6.9797L14.3656 11.8297C14.2498 11.9454 14.158 12.0828 14.0954 12.234C14.0328 12.3852 14.0005 12.5473 14.0005 12.711C14.0005 12.8746 14.0328 13.0367 14.0954 13.1879C14.158 13.3391 14.2498 13.4765 14.3656 13.5922C14.4813 13.7079 14.6187 13.7997 14.7699 13.8624C14.9211 13.925 15.0831 13.9572 15.2468 13.9572C15.4105 13.9572 15.5725 13.925 15.7237 13.8624C15.8749 13.7997 16.0123 13.7079 16.1281 13.5922L21.8656 7.8547C21.9814 7.73906 22.0734 7.6017 22.1361 7.45048C22.1988 7.29927 22.2311 7.13716 22.2311 6.97345C22.2311 6.80974 22.1988 6.64764 22.1361 6.49642C22.0734 6.34521 21.9814 6.20784 21.8656 6.0922L16.1281 0.354703C15.6531 -0.120297 14.8531 -0.120297 14.3656 0.367203Z');
      pathYellow.style.transition = 'all 0.3s ease-out';  
      pathYellow.style.transform = 'translateX(0px)';
      firstRectYellow.setAttribute('width', '12');
      firstRectYellow.style.transform = 'translateX(0px)';
      firstRectYellow.style.transition = 'all 0.3s ease-out';
    });
  });
}

arrowYellow();


  // arrow animation White

function arrowWhite() {
  const iconsWhite = document.querySelectorAll('#arrowWhite');
  
  iconsWhite.forEach(iconWhite => {
    const pathWhite = iconWhite.querySelector('svg path#arrowWhitePathOne');
    const firstRectWhite = iconWhite.querySelector('svg rect#arrowWhiteRectOne');
    
    iconWhite.addEventListener('mouseover', () => {
      pathWhite.setAttribute('d', 'M14.3656 0.367203C14.2497 0.482845 14.1577 0.620206 14.095 0.771422C14.0323 0.922639 14 1.08474 14 1.24845C14 1.41216 14.0323 1.57427 14.095 1.72548C14.1577 1.8767 14.2497 2.01406 14.3656 2.1297L19.2156 6.9797L14.3656 11.8297C14.2498 11.9454 14.158 12.0828 14.0954 12.234C14.0328 12.3852 14.0005 12.5473 14.0005 12.711C14.0005 12.8746 14.0328 13.0367 14.0954 13.1879C14.158 13.3391 14.2498 13.4765 14.3656 13.5922C14.4813 13.7079 14.6187 13.7997 14.7699 13.8624C14.9211 13.925 15.0831 13.9572 15.2468 13.9572C15.4105 13.9572 15.5725 13.925 15.7237 13.8624C15.8749 13.7997 16.0123 13.7079 16.1281 13.5922L21.8656 7.8547C21.9814 7.73906 22.0734 7.6017 22.1361 7.45048C22.1988 7.29927 22.2311 7.13716 22.2311 6.97345C22.2311 6.80974 22.1988 6.64764 22.1361 6.49642C22.0734 6.34521 21.9814 6.20784 21.8656 6.0922L16.1281 0.354703C15.6531 -0.120297 14.8531 -0.120297 14.3656 0.367203Z');
      pathWhite.style.transition = 'all 0.3s ease-out';
      pathWhite.style.transform = 'translateX(6px)';
      pathWhite.style.opacity = '1';
      firstRectWhite.setAttribute('width', '27');
      firstRectWhite.style.transform = 'translateX(-8px)';
      firstRectWhite.style.transition = 'all 0.3s ease-out';
    });
    
    iconWhite.addEventListener('mouseout', () => {
      pathWhite.setAttribute('d', 'M14.3656 0.367203C14.2497 0.482845 14.1577 0.620206 14.095 0.771422C14.0323 0.922639 14 1.08474 14 1.24845C14 1.41216 14.0323 1.57427 14.095 1.72548C14.1577 1.8767 14.2497 2.01406 14.3656 2.1297L19.2156 6.9797L14.3656 11.8297C14.2498 11.9454 14.158 12.0828 14.0954 12.234C14.0328 12.3852 14.0005 12.5473 14.0005 12.711C14.0005 12.8746 14.0328 13.0367 14.0954 13.1879C14.158 13.3391 14.2498 13.4765 14.3656 13.5922C14.4813 13.7079 14.6187 13.7997 14.7699 13.8624C14.9211 13.925 15.0831 13.9572 15.2468 13.9572C15.4105 13.9572 15.5725 13.925 15.7237 13.8624C15.8749 13.7997 16.0123 13.7079 16.1281 13.5922L21.8656 7.8547C21.9814 7.73906 22.0734 7.6017 22.1361 7.45048C22.1988 7.29927 22.2311 7.13716 22.2311 6.97345C22.2311 6.80974 22.1988 6.64764 22.1361 6.49642C22.0734 6.34521 21.9814 6.20784 21.8656 6.0922L16.1281 0.354703C15.6531 -0.120297 14.8531 -0.120297 14.3656 0.367203Z');
      pathWhite.style.transition = 'all 0.3s ease-out';  
      pathWhite.style.transform = 'translateX(0px)';
      firstRectWhite.setAttribute('width', '12');
      firstRectWhite.style.transform = 'translateX(0px)';
      firstRectWhite.style.transition = 'all 0.3s ease-out';
    });
  });
}

arrowWhite();

// short to long arrow:

function shortArrow(){
  const iconsShortArrow = document.querySelectorAll('#shortArrow');

  iconsShortArrow.forEach(iconshortArrow => {
    const pathshortArrow = iconshortArrow.querySelector('svg path#shortArrowPath');
    const shortArrowRect = iconshortArrow.querySelector('svg rect#shortArrowRect');
    
    iconshortArrow.addEventListener('mouseover', () => {
     pathshortArrow.style.transition = 'all 0.3s ease-out';
      pathshortArrow.style.transform = 'translateX(6px)';
      pathshortArrow.style.opacity = '1';
      shortArrowRect.setAttribute('width', '20');
      shortArrowRect.style.transform = 'translateX(-6px)';
      shortArrowRect.style.transition = 'all 0.3s ease-out';
    });
    
    iconshortArrow.addEventListener('mouseout', () => {
      pathshortArrow.style.transition = 'all 0.3s ease-out';  
      pathshortArrow.style.transform = 'translateX(0px)';
      shortArrowRect.setAttribute('width', '8');
      shortArrowRect.style.transform = 'translateX(0px)';
      shortArrowRect.style.transition = 'all 0.3s ease-out';
    });
  });
}

shortArrow();




// ninja form
jQuery(document).on('nfFormReady', function (e, layoutView) {
  var nfJoinButtons = document.querySelectorAll('#nf-field-14, #nf-field-8');
  var customJoinButton = document.createElement('button');
  customJoinButton.classList.add('shortArrow');
  customJoinButton.id = 'shortArrow';

  var anchorJoin = document.createElement('a');
  anchorJoin.href = '';

  var svgJoin = document.createElementNS('http://www.w3.org/2000/svg', 'svg');
  svgJoin.setAttribute('width', '25');
  svgJoin.setAttribute('height', '11');
  svgJoin.setAttribute('viewBox', '0 0 12 14');
  svgJoin.setAttribute('fill', 'none');

  var pathJoin = document.createElementNS('http://www.w3.org/2000/svg', 'path');
  pathJoin.id = 'shortArrowpath';
  pathJoin.setAttribute('d', 'M3.36556 0.367203C3.24968 0.482845 3.15774 0.620206 3.09501 0.771422C3.03229 0.922639 3 1.08474 3 1.24845C3 1.41216 3.03229 1.57427 3.09501 1.72548C3.15774 1.8767 3.24968 2.01406 3.36556 2.1297L8.21556 6.9797L3.36556 11.8297C3.24983 11.9454 3.15803 12.0828 3.0954 12.234C3.03277 12.3852 3.00053 12.5473 3.00053 12.711C3.00053 12.8746 3.03277 13.0367 3.0954 13.1879C3.15803 13.3391 3.24983 13.4765 3.36556 13.5922C3.48128 13.7079 3.61867 13.7997 3.76988 13.8624C3.92108 13.925 4.08314 13.9572 4.24681 13.9572C4.41047 13.9572 4.57253 13.925 4.72373 13.8624C4.87494 13.7997 5.01233 13.7079 5.12806 13.5922L10.8656 7.8547C10.9814 7.73906 11.0734 7.6017 11.1361 7.45048C11.1988 7.29927 11.2311 7.13716 11.2311 6.97345C11.2311 6.80974 11.1988 6.64764 11.1361 6.49642C11.0734 6.34521 10.9814 6.20784 10.8656 6.0922L5.12806 0.354703C4.65306 -0.120297 3.85306 -0.120297 3.36556 0.367203Z');
  pathJoin.setAttribute('fill', 'var(--secondary)');

  var rect = document.createElementNS('http://www.w3.org/2000/svg', 'rect');
  rect.id = 'shortArrowRect';
  rect.setAttribute('y', '6');
  rect.setAttribute('width', '10');
  rect.setAttribute('height', '2');
  rect.setAttribute('fill', 'var(--secondary)');
  rect.setAttribute('transform', 'translate(0, 0)');

  var btnJoinText = document.createElement('p');
  btnJoinText.classList.add('btn-text');
  
customJoinButton.addEventListener('mouseenter', function () {
  var rect = this.querySelector('#shortArrowRect');
  rect.setAttribute('width', '20');
  rect.setAttribute('transition', 'all 0.3s ease-out');
  rect.setAttribute('transform', 'translate(-10, 0)');
});

customJoinButton.addEventListener('mouseleave', function () {
  var rect = this.querySelector('#shortArrowRect');
  rect.setAttribute('width', '10');
  rect.setAttribute('transition', 'all 0.3s ease-out');
  rect.setAttribute('transform', 'translate(0, 0)');
});
  
  for (var i = 0; i < nfJoinButtons.length; i++) {
    if (nfJoinButtons[i].id === 'nf-field-8') {
      btnJoinText.textContent = 'Send';
      btnJoinText.style.paddingTop = '18%';
    } else {
      btnJoinText.textContent = 'Join Our Community';
      btnJoinText.style.paddingTop = '8%';
    }
    
    svgJoin.appendChild(pathJoin);
    svgJoin.appendChild(rect);
    anchorJoin.appendChild(svgJoin);
    anchorJoin.appendChild(btnJoinText);
    customJoinButton.appendChild(anchorJoin);
    nfJoinButtons[i].parentNode.replaceChild(customJoinButton, nfJoinButtons[i]);
    customJoinButton = document.createElement('button');
    customJoinButton.classList.add('shortArrow');
    customJoinButton.id = 'shortArrow';
    anchorJoin = document.createElement('a');
    anchorJoin.href = '';
    svgJoin = document.createElementNS('http://www.w3.org/2000/svg', 'svg');
    svgJoin.setAttribute('width', '25');
    svgJoin.setAttribute('height', '11');
    svgJoin.setAttribute('viewBox', '0 0 12 14');
    svgJoin.setAttribute('fill', 'none');
    pathJoin = document.createElementNS('http://www.w3.org/2000/svg', 'path');
    pathJoin.id = 'shortArrowpath';
    pathJoin.setAttribute('d', 'M3.36556 0.367203C3.24968 0.482845 3.15774 0.620206 3.09501 0.771422C3.03229 0.922639 3 1.08474 3 1.24845C3 1.41216 3.03229 1.57427 3.09501 1.72548C3.15774 1.8767 3.24968 2.01406 3.36556 2.1297L8.21556 6.9797L3.36556 11.8297C3.24983 11.9454 3.15803 12.0828 3.0954 12.234C3.03277 12.3852 3.00053 12.5473 3.00053 12.711C3.00053 12.8746 3.03277 13.0367 3.0954 13.1879C3.15803 13.3391 3.24983 13.4765 3.36556 13.5922C3.48128 13.7079 3.61867 13.7997 3.76988 13.8624C3.92108 13.925 4.08314 13.9572 4.24681 13.9572C4.41047 13.9572 4.57253 13.925 4.72373 13.8624C4.87494 13.7997 5.01233 13.7079 5.12806 13.5922L10.8656 7.8547C10.9814 7.73906 11.0734 7.6017 11.1361 7.45048C11.1988 7.29927 11.2311 7.13716 11.2311 6.97345C11.2311 6.80974 11.1988 6.64764 11.1361 6.49642C11.0734 6.34521 10.9814 6.20784 10.8656 6.0922L5.12806 0.354703C4.65306 -0.120297 3.85306 -0.120297 3.36556 0.367203Z');
    pathJoin.setAttribute('fill', 'var(--secondary)');
    rect = document.createElementNS('http://www.w3.org/2000/svg', 'rect');
    rect.id = 'shortArrowRect';
    rect.setAttribute('y', '6');
    rect.setAttribute('width', '10');
    rect.setAttribute('height', '2');
    rect.setAttribute('fill', 'var(--secondary)');
    rect.setAttribute('transform', 'translate(0, 0)');
    btnJoinText = document.createElement('p');
    btnJoinText.classList.add('btn-text');
  }
});