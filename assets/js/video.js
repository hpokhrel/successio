const videos = document.querySelectorAll('#myVideo');
videos.forEach(video => {
  video.addEventListener('click', () => {
    const videoModal = `
      <div class="modal fade" id="videoModal" tabindex="-1" aria-labelledby="videoModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered modal-lg">
          <div class="modal-content">
            <div class="modal-button">
              <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
              <video autoplay controls>
                // <source src="${video.src}" type="video/mp4">
                <source src="assets/img/vid2.mp4" type="video/mp4">
              </video>
            </div>
          </div>
        </div>
      </div>
    `;
    document.querySelector('.video_page').insertAdjacentHTML('afterend', videoModal);
    const modal = new bootstrap.Modal(document.getElementById('videoModal'));
    modal.show();
  });
});
