// jQuery(document).on('nfFormReady', function (e, layoutView) {
//   var nfSendButton = document.querySelector('#nf-field-8');
//   var customSendButton = document.createElement('button');
//   customSendButton.classList.add('shortArrow');
//   customSendButton.id = 'shortArrow';

//   var anchorSend = document.createElement('a');
//   anchorSend.href = '';

//   var svgSend = document.createElementNS('http://www.w3.org/2000/svg', 'svg');
//   svgSend.setAttribute('width', '25');
//   svgSend.setAttribute('height', '14');
//   svgSend.setAttribute('viewBox', '0 0 12 14');
//   svgSend.setAttribute('fill', 'none');

//   var pathSend = document.createElementNS('http://www.w3.org/2000/svg', 'path');
//   pathSend.id = 'shortArrowpath';
//   pathSend.setAttribute('d', 'M3.36556 0.367203C3.24968 0.482845 3.15774 0.620206 3.09501 0.771422C3.03229 0.922639 3 1.08474 3 1.24845C3 1.41216 3.03229 1.57427 3.09501 1.72548C3.15774 1.8767 3.24968 2.01406 3.36556 2.1297L8.21556 6.9797L3.36556 11.8297C3.24983 11.9454 3.15803 12.0828 3.0954 12.234C3.03277 12.3852 3.00053 12.5473 3.00053 12.711C3.00053 12.8746 3.03277 13.0367 3.0954 13.1879C3.15803 13.3391 3.24983 13.4765 3.36556 13.5922C3.48128 13.7079 3.61867 13.7997 3.76988 13.8624C3.92108 13.925 4.08314 13.9572 4.24681 13.9572C4.41047 13.9572 4.57253 13.925 4.72373 13.8624C4.87494 13.7997 5.01233 13.7079 5.12806 13.5922L10.8656 7.8547C10.9814 7.73906 11.0734 7.6017 11.1361 7.45048C11.1988 7.29927 11.2311 7.13716 11.2311 6.97345C11.2311 6.80974 11.1988 6.64764 11.1361 6.49642C11.0734 6.34521 10.9814 6.20784 10.8656 6.0922L5.12806 0.354703C4.65306 -0.120297 3.85306 -0.120297 3.36556 0.367203Z');
//   pathSend.setAttribute('fill', 'black');

//   var rect = document.createElementNS('http://www.w3.org/2000/svg', 'rect');
//   rect.id = 'shortArrowRect';
//   rect.setAttribute('y', '6');
//   rect.setAttribute('width', '10');
//   rect.setAttribute('height', '2');
//   rect.setAttribute('fill', 'black');
//   rect.setAttribute('transform', 'translate(0, 0)');

//   var btnSendText = document.createElement('p');
//   btnSendText.classList.add('btn-text');
//   btnSendText.textContent = 'Send Our Community';
//   btnSendText.style.paddingTop = '8%';

//   svgSend.appendChild(pathSend);
//   svgSend.appendChild(rect);
//   anchorSend.appendChild(svgSend);
//   anchorSend.appendChild(btnSendText);
//   customSendButton.appendChild(anchorSend);
//   nfSendButton.parentNode.replaceChild(customSendButton, nfSendButton);
  
//     customSendButton.addEventListener('mouseenter', function () {
//     rect.setAttribute('width', '20');
//     rect.setAttribute('transition', 'all 0.3s ease-out');
//     rect.setAttribute('transform', 'translate(-10, 0)');
//   });

//   customSendButton.addEventListener('mouseleave', function () {
//     rect.setAttribute('width', '10');
//     rect.setAttribute('transition', 'all 0.3s ease-out');
//     rect.setAttribute('transform', 'translate(0, 0)');
//   });
// });



jQuery(document).on('nfFormReady', function (e, layoutView) {
  var nfJoinButton = document.querySelectorAll('#nf-field-14');
  var customJoinButton = document.createElement('button');
  customJoinButton.classList.add('shortArrow');
  customJoinButton.id = 'shortArrow';

  var anchorJoin = document.createElement('a');
  anchorJoin.href = '';

  var svgJoin = document.createElementNS('http://www.w3.org/2000/svg', 'svg');
  svgJoin.setAttribute('width', '25');
  svgJoin.setAttribute('height', '14');
  svgJoin.setAttribute('viewBox', '0 0 12 14');
  svgJoin.setAttribute('fill', 'none');

  var pathJoin = document.createElementNS('http://www.w3.org/2000/svg', 'path');
  pathJoin.id = 'shortArrowpath';
  pathJoin.setAttribute('d', 'M3.36556 0.367203C3.24968 0.482845 3.15774 0.620206 3.09501 0.771422C3.03229 0.922639 3 1.08474 3 1.24845C3 1.41216 3.03229 1.57427 3.09501 1.72548C3.15774 1.8767 3.24968 2.01406 3.36556 2.1297L8.21556 6.9797L3.36556 11.8297C3.24983 11.9454 3.15803 12.0828 3.0954 12.234C3.03277 12.3852 3.00053 12.5473 3.00053 12.711C3.00053 12.8746 3.03277 13.0367 3.0954 13.1879C3.15803 13.3391 3.24983 13.4765 3.36556 13.5922C3.48128 13.7079 3.61867 13.7997 3.76988 13.8624C3.92108 13.925 4.08314 13.9572 4.24681 13.9572C4.41047 13.9572 4.57253 13.925 4.72373 13.8624C4.87494 13.7997 5.01233 13.7079 5.12806 13.5922L10.8656 7.8547C10.9814 7.73906 11.0734 7.6017 11.1361 7.45048C11.1988 7.29927 11.2311 7.13716 11.2311 6.97345C11.2311 6.80974 11.1988 6.64764 11.1361 6.49642C11.0734 6.34521 10.9814 6.20784 10.8656 6.0922L5.12806 0.354703C4.65306 -0.120297 3.85306 -0.120297 3.36556 0.367203Z');
  pathJoin.setAttribute('fill', 'black');

  var rect = document.createElementNS('http://www.w3.org/2000/svg', 'rect');
  rect.id = 'shortArrowRect';
  rect.setAttribute('y', '6');
  rect.setAttribute('width', '10');
  rect.setAttribute('height', '2');
  rect.setAttribute('fill', 'black');
  rect.setAttribute('transform', 'translate(0, 0)');

  var btnJoinText = document.createElement('p');
  btnJoinText.classList.add('btn-text');
  btnJoinText.textContent = 'Join Our Community';
  btnJoinText.style.paddingTop = '8%';

  svgJoin.appendChild(pathJoin);
  svgJoin.appendChild(rect);
  anchorJoin.appendChild(svgJoin);
  anchorJoin.appendChild(btnJoinText);
  customJoinButton.appendChild(anchorJoin);
  nfJoinButton.parentNode.replaceChild(customJoinButton, nfJoinButton);
  
    customJoinButton.addEventListener('mouseenter', function () {
    rect.setAttribute('width', '20');
    rect.setAttribute('transition', 'all 0.3s ease-out');
    rect.setAttribute('transform', 'translate(-10, 0)');
  });

  customJoinButton.addEventListener('mouseleave', function () {
    rect.setAttribute('width', '10');
    rect.setAttribute('transition', 'all 0.3s ease-out');
    rect.setAttribute('transform', 'translate(0, 0)');
  });
});