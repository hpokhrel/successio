// Get the parent element of the Ninja Form button
var nfButtonParent = document.querySelector('.nf-field-element');

// Create a new button element
var customButton = document.createElement('button');
customButton.classList.add('shortArrow');
customButton.id = 'shortArrow';

// Create a new anchor element
var anchor = document.createElement('a');
anchor.href = '';

// Create a new SVG element
var svg = document.createElementNS('http://www.w3.org/2000/svg', 'svg');
svg.setAttribute('width', '25');
svg.setAttribute('height', '14');
svg.setAttribute('viewBox', '0 0 12 14');
svg.setAttribute('fill', 'none');

// Create a new path element
var path = document.createElementNS('http://www.w3.org/2000/svg', 'path');
path.id = 'shortArrowPath';
path.setAttribute('d', 'M3.36556 0.367203C3.24968 0.482845 3.15774 0.620206 3.09501 0.771422C3.03229 0.922639 3 1.08474 3 1.24845C3 1.41216 3.03229 1.57427 3.09501 1.72548C3.15774 1.8767 3.24968 2.01406 3.36556 2.1297L8.21556 6.9797L3.36556 11.8297C3.24983 11.9454 3.15803 12.0828 3.0954 12.234C3.03277 12.3852 3.00053 12.5473 3.00053 12.711C3.00053 12.8746 3.03277 13.0367 3.0954 13.1879C3.15803 13.3391 3.24983 13.4765 3.36556 13.5922C3.48128 13.7079 3.61867 13.7997 3.76988 13.8624C3.92108 13.925 4.08314 13.9572 4.24681 13.9572C4.41047 13.9572 4.57253 13.925 4.72373 13.8624C4.87494 13.7997 5.01233 13.7079 5.12806 13.5922L10.8656 7.8547C10.9814 7.73906 11.0734 7.6017 11.1361 7.45048C11.1988 7.29927 11.2311 7.13716 11.2311 6.97345C11.2311 6.80974 11.1988 6.64764 11.1361 6.49642C11.0734 6.34521 10.9814 6.20784 10.8656 6.0922L5.12806 0.354703C4.65306 -0.120297 3.85306 -0.120297 3.36556 0.367203Z');
path.setAttribute('fill', 'black');

// Create a new rect
var rect = document.createElementNS('http://www.w3.org/2000/svg', 'rect');
rect.setAttribute('x', '0');
rect.setAttribute('y', '0');
rect.setAttribute('width', '25');
rect.setAttribute('height', '14');
rect.setAttribute('fill', 'none');

// Append the path and rect elements to the SVG element
svg.appendChild(path);
svg.appendChild(rect);

// Append the SVG element to the anchor element
anchor.appendChild(svg);

// Append the anchor element to the custom button element
customButton.appendChild(anchor);

// Replace the Ninja Form button with the custom button
nfButtonParent.replaceChild(customButton, nfButton);

// Add an event listener to the custom button
customButton.addEventListener('click', function() {
// Code to execute when the custom button is clicked
});